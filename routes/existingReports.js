var express = require('express');
var router = express.Router();

router
  .get('/', function(req, res) {
    res.render("existing-reports");
  });

module.exports = router;
