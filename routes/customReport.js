var express = require('express');
var router = express.Router();

const {
  Pool,
  Client
} = require('pg');
const conn = 'postgresql://postgres:postgres@localhost:5432/inventory-test-DB'

router.get('/', function(req, res) {
  res.render("custom-report");
})

router.post('/', function(req, res) {
  var customized;
  console.log(req.body.building + "<=")

  if (req.body.requested === "equipment-multi") {
    if (req.body.building == undefined && req.body.number == '') {
      customized = {
        text: 'SELECT classroom.class_building, classroom.class_num, equipment.equip_type FROM classroom INNER JOIN classequipment ON classroom.classroom_id = classequipment.classroom_id INNER JOIN equipment ON classequipment.equip_id = equipment.equip_id AND equipment.equip_type = $1',
        values: [req.body.equipment]
      }
    }

    if (req.body.building != undefined && req.body.number == '') {
      customized = {
        text: 'SELECT classroom.class_building, classroom.class_num, equipment.equip_type FROM classroom INNER JOIN classequipment ON classroom.classroom_id = classequipment.classroom_id AND classroom.class_building = $1 INNER JOIN equipment ON classequipment.equip_id = equipment.equip_id AND equipment.equip_type = $2',
        values: [req.body.building, req.body.equipment]
      }
    }

    if (req.body.building == undefined && req.body.number != '') {
      customized = {
        text: 'SELECT classroom.class_building, classroom.class_num, equipment.equip_type FROM classroom INNER JOIN classequipment ON classroom.classroom_id = classequipment.classroom_id AND classroom.class_num = $1 INNER JOIN equipment ON classequipment.equip_id = equipment.equip_id AND equipment.equip_type = $2',
        values: [req.body.number, req.body.equipment]
      }
    }

    if (req.body.building != undefined && req.body.number != '') {
      customized = {
        text: 'SELECT classroom.class_building, classroom.class_num, equipment.equip_type FROM classroom INNER JOIN classequipment ON classroom.classroom_id = classequipment.classroom_id AND classroom.class_building = $1 AND classroom.class_num = $2 INNER JOIN equipment ON classequipment.equip_id = equipment.equip_id AND equipment.equip_type = $3',
        values: [req.body.building, req.body.number, req.body.equipment]
      }
    }
  }

  if (req.body.requested === "software-multi") {
    console.log("SOFTWARE-MULTI")

    if (req.body.building == undefined && req.body.number == '') {
      // TODO:
    }

    if (req.body.building != undefined && req.body.number == '') {

    }

    if (req.body.building == undefined && req.body.number != '') {

    }

    if (req.body.building != undefined && req.body.number != '') {

    }
  }

  if (req.body.requested.length == 2) {
    console.log("BOTH FIELDS SELECTED");

    if (req.body.building == undefined && req.body.number == '') {

    }

    if (req.body.building != undefined && req.body.number == '') {

    }

    if (req.body.building == undefined && req.body.number != '') {

    }

    if (req.body.building != undefined && req.body.number != '') {

    }
  }

  const client = new Client({
    connectionString: conn,
  })

  client.connect()

  console.log(customized)
  console.log("after")

  client.query(customized)
    .then(customized => {
      console.log(customized)
      res.render('custom-report', {
        custom: customized.rows
      })
    })

    .catch(e => console.error(e.stack))
})

module.exports = router;
