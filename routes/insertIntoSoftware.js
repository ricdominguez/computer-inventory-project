var express = require('express');
var router = express.Router();

var manageSoftware = require('../controllers/manageSoftware.js');

router
  .post('/', function(req, res) {
    manageSoftware.manageSoftware(req, res);
  })

module.exports = router;
