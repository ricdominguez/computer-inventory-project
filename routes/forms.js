var express = require('express');
var router = express.Router();

const {Pool, Client} = require('pg');
const conn = 'postgresql://postgres:postgres@localhost:5432/inventory-test-DB'

const querystring = require('querystring');

router.get('/', function(req, res) {
  var selectAllClassroom = 'SELECT * FROM classroom ORDER BY class_building, class_num';
  var selectAllEquipment = 'SELECT * FROM equipment ORDER BY equip_type';
  var selectAllSoftware = 'SELECT * FROM software ORDER BY software_name';

  const client = new Client({
    connectionString: conn,
  })

  client.connect();

  client.query(selectAllClassroom)
    .then(classroom => {
      client.query(selectAllEquipment)
        .then(equipment => {
          client.query(selectAllSoftware)
            .then(software => {
              console.log(classroom.rows)
              console.log(equipment.rows)
              console.log(software.rows)

              res.render('forms', {
                classroom: classroom.rows,
                equipment: equipment.rows,
                software: software.rows });
            })

            .catch(e => console.error(e.stack))
        })

        .catch(e => console.error(e.stack))
    })

    .catch(e => console.error(e.stack))
});

router.post('/', function(req, res) {
  const arg = querystring.stringify ({
    "identifier": req.body.crid
  })

  res.redirect('classroom-info?' + arg);
});

module.exports = router;
