var express = require('express');
var router = express.Router();

const {Pool, Client} = require('pg');
const conn = 'postgresql://postgres:postgres@localhost:5432/inventory-test-DB'

router
  .get('/', function(req, res) {
    console.log(req.query.identifier);
    const classroomQuery = {
      text: 'SELECT * FROM classroom WHERE classroom_id = ($1)',
      values: [req.query.identifier]
    }

    const softwareQuery = {
      text: 'SELECT software.software_name FROM classsoftware INNER JOIN software ON classsoftware.software_id = software.software_id AND classsoftware.classroom_id = ($1)',
      values: [req.query.identifier]
    }

    const equipmentQuery = {
      text: 'SELECT equipment.equip_type FROM classequipment INNER JOIN equipment ON classequipment.equip_id = equipment.equip_id AND classequipment.classroom_id = ($1)',
      values: [req.query.identifier]
    }

    const client = new Client({
      connectionString: conn,
    })

    client.connect()

    client.query(classroomQuery)
      .then(classroom => {
        client.query(softwareQuery)
          .then(software => {
            client.query(equipmentQuery)
              .then(equipment => {
                console.log(classroom.rows)
                console.log(equipment.rows)
                console.log(software.rows)

                res.render('classroom-info', {
                  classroom: classroom.rows,
                  software: software.rows,
                  equipment: equipment.rows });
              })

              .catch(e => console.error(e.stack))
          })

          .catch(e => console.error(e.stack))
      })

      .catch(e => console.error(e.stack))
  });

module.exports = router;
