var express = require('express');
var router = express.Router();

var manageEquipment = require('../controllers/manageEquipment.js');

router
  .post('/', function(req, res) {
    manageEquipment.manageEquipment(req, res);
  })

module.exports = router;
