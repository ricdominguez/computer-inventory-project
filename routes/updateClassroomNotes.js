var express = require('express');
var router = express.Router();

const {Pool, Client} = require('pg');
const conn = 'postgresql://postgres:postgres@localhost:5432/inventory-test-DB'

const querystring = require('querystring');

router
  .post('/', function(req, res) {
    console.log(req.body.notes);
    console.log(req.body.identifier);

    const query = {
      text: 'UPDATE classroom SET notes = $1 WHERE classroom_id = $2',
      values: [req.body.notes, req.body.identifier]
    }

    const client = new Client({
      connectionString: conn,
    })

    client.connect()

    client.query(query)
      .then(query => {
        const arg = querystring.stringify ({
          "identifier": req.body.identifier
        })

        res.redirect('classroom-info?' + arg)
      })

      .catch(e => console.error(e.stack))
  })

module.exports = router;
