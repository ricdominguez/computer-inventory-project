var express = require('express');
var router = express.Router();

var manageClassroom = require('../controllers/manageClassroom.js');

router
  .post('/', function(req, res) {
    manageClassroom.manageClassroom(req, res);
  })

module.exports = router;
