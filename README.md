To start, run these commands in the directory of the project.

Windows:
  set DEBUG=invproj:* & npm start

Mac/Linux:
  DEBUG=invproj:* npm start
