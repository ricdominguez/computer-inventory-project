const {Pool, Client} = require('pg');
const conn = 'postgresql://postgres:postgres@localhost:5432/inventory-test-DB'

module.exports = {
  insertSoftware: function(req, res) {
    const query = {
      text: 'INSERT INTO software (software_name) VALUES ($1)',
      values: [req.body.software]
    }

    const client = new Client({
      connectionString: conn,
    })

    client.connect()

    client.query(query)
      .then(query => res.redirect('forms'))
      .catch(e => console.error(e.stack))
  }
}
