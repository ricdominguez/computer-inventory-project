const {Pool, Client} = require('pg');
const conn = 'postgresql://postgres:postgres@localhost:5432/inventory-test-DB'

module.exports = {
  insertClassroom: function(req, res) {
    const query = {
      text: 'INSERT INTO classroom (class_num, class_building, notes) VALUES ($1, $2, $3)',
      values: [req.body.number, req.body.building, req.body.notes]
    }

    const client = new Client({
      connectionString: conn,
    })

    client.connect()

    client.query(query)
      .then(query => res.redirect('forms'))
      .catch(e => console.error(e.stack))
  }
}
