const {Pool, Client} = require('pg');
const conn = 'postgresql://postgres:postgres@localhost:5432/inventory-test-DB'

module.exports = {
  insertEquipment: function(req, res) {
    const query = {
      text: 'INSERT INTO equipment (equip_type) VALUES ($1)',
      values: [req.body.type]
    }

    const client = new Client({
      connectionString: conn,
    })

    client.connect()

    client.query(query)
      .then(query => res.redirect('forms'))
      .catch(e => console.error(e.stack))
  }
}
