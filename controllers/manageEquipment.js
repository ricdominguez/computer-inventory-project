var insertEquipment = require('../models/insertEquipment.js');

module.exports = {
  manageEquipment: function(req, res) {
    insertEquipment.insertEquipment(req, res);
  }
}
