$(document).ready(function() {
  $("#add-classroom").click(function() {
    $("#classroom-modal").modal();
  });

  $("#add-equipment").click(function() {
    $("#equipment-modal").modal();
  });

  $("#add-software").click(function() {
    $("#software-modal").modal();
  });
});
