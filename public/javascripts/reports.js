$(document).ready(function() {
  $("#specifier-classroom").prop('hidden', true);
  $("#specifier-building").prop('hidden', true);

  $("#specifier-lookingfor-label").prop('hidden', true);
  $("#specifier-lookingfor").prop('hidden', true);

  $("#specifier-equipment").prop('hidden', true);
  $("#specifier-software").prop('hidden', true);

  $("custom-report-btn").prop('hidden', false);


  $("#to-query").change(function() {
    var selected = $("#to-query").val();

    if (selected === "classroom") {
      $("#specifier-classroom").prop('hidden', false);
      $("#specifier-building").prop('hidden', false);

      $("#specifier-lookingfor-label").prop('hidden', false);
      $("#specifier-lookingfor").prop('hidden', false);

      $("#specifier-equipment").prop('hidden', true);
      $("#specifier-software").prop('hidden', true);
    }
  });

  $("#specifier-lookingfor").change(function() {
    var lookingfor = $("#specifier-lookingfor").val();

    if (lookingfor.length == 1 && lookingfor == "equipment-multi") {
      $("#specifier-equipment").prop('hidden', false);
      $("#specifier-software").prop('hidden', true);

      $("#specifier-equipment").prop('required', true);
      $("#specifier-software").prop('required', false);
    }

    if (lookingfor.length == 1 && lookingfor == "software-multi") {
      $("#specifier-equipment").prop('hidden', true);
      $("#specifier-software").prop('hidden', false);

      $("#specifier-equipment").prop('required', false);
      $("#specifier-software").prop('required', true);
    }

    if (lookingfor.length == 2) {
      $("#specifier-equipment").prop('hidden', false);
      $("#specifier-software").prop('hidden', false);

      $("#specifier-equipment").prop('required', true);
      $("#specifier-software").prop('required', true);
    }
  });

  function printTable() {
    var doc = document.getElementById("printable").innerHTML;

    win = window.open("", "_blank", "");

    win.document.open();
    win.document.write("<html lang='en'><head><title>Print Custom Report</title><link rel='stylesheet' href='https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css' integrity='sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T' crossorigin='anonymous'></head><body>");
    win.document.write("<br><br>")
    win.document.write(doc);
    win.document.write("</body></html>");

    setTimeout(function() {
      win.print();
      win.close();
    }, 10);
  }

  $("#print-btn").click(function() {
    printTable();
  });
});
