$(document).ready(function() {
  $("#edit-notes").click(function() {
    $("textarea").prop('disabled', false);
    $("#edit-notes").prop('hidden', true);
    $("#cancel-edit").prop('hidden', false);
    $("#submit-notes").prop('hidden', false);
  });

  $("#cancel-edit").click(function() {
    $("textarea").prop('disabled', true);
    $("#edit-notes").prop('hidden', false);
    $("#cancel-edit").prop('hidden', true);
    $("#submit-notes").prop('hidden', true);
  });
});
