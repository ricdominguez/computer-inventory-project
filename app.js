var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var bodyParser = require('body-parser');

var indexRouter = require('./routes/index');
var reportsRouter = require('./routes/reports');
var createReportRouter = require('./routes/createReport');
var existingReportsRouter = require('./routes/existingReports');
var formsRouter = require('./routes/forms');
var customReportRouter = require('./routes/customReport');

var insertIntoClassroomRouter = require('./routes/insertIntoClassroom');
var insertIntoEquipmentRouter = require('./routes/insertIntoEquipment');
var insertIntoSoftwareRouter = require('./routes/insertIntoSoftware');

var updateClassroomNotesRouter = require('./routes/updateClassroomNotes');

var classroomInfoRouter = require('./routes/classroomInfo');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

// Dependencies added
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.use('/', indexRouter);
app.use('/reports', reportsRouter);
app.use('/create-report', createReportRouter);
app.use('/existing-reports', existingReportsRouter);
app.use('/forms', formsRouter);
app.use('/custom-report', customReportRouter);

app.use('/insertIntoClassroom', insertIntoClassroomRouter);
app.use('/insertIntoEquipment', insertIntoEquipmentRouter);
app.use('/insertIntoSoftware', insertIntoSoftwareRouter);

app.use('/updateClassroomNotes', updateClassroomNotesRouter);

app.use('/classroom-info', classroomInfoRouter);
//app.use('/users', usersRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
